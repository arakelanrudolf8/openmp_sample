#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <memory.h>
#include <math.h>
#include <omp.h>

static const int iLoopLen = 10000000;
static const int iFactor = 1000;
static const int iArrLen = 10000;

void print_time_delta(DWORD tcStart, DWORD tcEnd)
{
	const double dTime = (tcEnd - tcStart) * .001;
	printf("%g s\n", dTime);
}


void f1(double* pdArr)
{
	for (int i = 0; i < iLoopLen; ++i)
	{
		const int index = i % iArrLen;
		pdArr[index] += fabs(i * .001 - 20.) * 5. + 3.;
	}
}

void f2(double* pdArr)
{
	const int numThreads = 8;
	#pragma omp parallel shared(pdArr) num_threads(numThreads) 
	{
		int draft = iArrLen / numThreads + 1;
		for (int i = omp_get_thread_num() * draft; i < (omp_get_thread_num() + 1) * draft && i < iArrLen; ++i)
		{
			for (int j = i; j < iLoopLen; j += iArrLen)
				pdArr[j % iArrLen] += fabs(j * .001 - 20.) * 5. + 3.;
		}
	}
}

bool is_equal(double* pdArr1, double* pdArr2)
{
	for (int i = 0; i < iArrLen; ++i)
		if (pdArr1[i] != pdArr2[i])
			return false;

	return true;
}


int _tmain(int argc, _TCHAR* argv[])
{
	double dArr1[iArrLen], dArr2[iArrLen];
	memset(dArr1, 0, sizeof(dArr1));
	memset(dArr2, 0, sizeof(dArr2));

	printf("*** Test1: 1 thread ");
	{
		const DWORD tcStart = GetTickCount();
		for (int i = 0; i < iFactor; ++i)
		{
			f1(dArr1);
		}
		const DWORD tcEnd = GetTickCount();
		print_time_delta(tcStart, tcEnd);
	}

	printf("*** Test2: 8 threads (critical) ");
	{
		const DWORD tcStart = GetTickCount();
		for (int i = 0; i < iFactor; ++i)
		{
			f2(dArr2);
		}
		const DWORD tcEnd = GetTickCount();
		print_time_delta(tcStart, tcEnd);
	}

	printf("*** Equality test: %s", is_equal(dArr1, dArr2) ? "true" : "false");
}